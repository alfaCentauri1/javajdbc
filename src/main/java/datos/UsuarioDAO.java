package datos;

import domain.Usuario;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static datos.Conexion.getConnection;

public class UsuarioDAO extends DAO {
    private static final String SQL_INSERT = "INSERT INTO usuario (usuario, password) VALUES (?,?);";
    private static final String SQL_SELECT = "SELECT id, usuario, password FROM usuario;";
    private static final String SQL_UPDATE = "UPDATE usuario SET nombre = ?, password = ? WHERE id = ?";
    private static final String SQL_DELETE = "DELETE FROM usuario WHERE id=?;";
    private static final String SQL_SELECT_FIND_BY_ID = "SELECT id, nombre, password FROM usuario WHERE id=? ORDER BY id ASC;";
    private Usuario usuario;    
    private List<Usuario> usuarios;
    @Override
    public int insertar(Object nuevo) {
        int registros = 0;
        usuario = (Usuario) nuevo;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, usuario.getUsuario());
            preparedStatement.setString(2, usuario.getPassword());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al insertar el registro: " + usuario + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public List listar() {
        usuarios = new ArrayList<>();
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT);
            resultado = preparedStatement.executeQuery();
            while (resultado.next()){
                usuario = new Usuario();
                usuario.setId(resultado.getInt("id"));
                usuario.setUsuario(resultado.getString("usuario"));
                usuario.setPassword(resultado.getString("password"));
                usuarios.add(usuario);
            }
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConextions();
        }
        return usuarios;
    }

    @Override
    public int actualizar(Object objeto) {
        int registros = 0;
        usuario = (Usuario) objeto;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_UPDATE);
            preparedStatement.setString(1, usuario.getUsuario());
            preparedStatement.setString(2, usuario.getPassword());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro: " + usuario + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public int eliminar(int id) {
        int registros = 0;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_DELETE);
            preparedStatement.setInt(1, id);
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro de indice: " + id + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public Object buscarPorId(int id) {
        usuario = new Usuario();
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, id);
            this.getFielsTable();
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConextions();
        }
        return usuario;
    }

    private void getFielsTable() throws Exception{
        resultado = preparedStatement.executeQuery();
        while (resultado.next()){
            usuario.setId(resultado.getInt("id"));
            usuario.setUsuario(resultado.getString("usuario"));
            usuario.setPassword(resultado.getString("password"));
        }
    }

    @Override
    public Object buscar(Object objeto) {
        usuario = (Usuario) objeto;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, usuario.getId());
            this.getFielsTable();
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConextions();
        }
        return usuario;
    }

    private void closeConextions(){
        try{
            Conexion.close(resultado);
            Conexion.close(preparedStatement);
            Conexion.close(conexion);
        }catch (Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
    }
}
