package datos;

import static datos.Conexion.*;
import domain.Persona;
import java.sql.*;
import java.util.*;

public class PersonaDAO implements ITypeDAO{
    private static final String SQL_SELECT = "SELECT id_persona, nombre, apellido, email, telefono FROM persona;";
    private static final String SQL_INSERT = "INSERT INTO persona (nombre, apellido, email, telefono) VALUES (?,?,?,?);";
    private static final String SQL_UPDATE = "UPDATE persona SET nombre = ?, apellido = ?, email = ?, telefono = ? WHERE id_persona = ?";
    private static final String SQL_DELETE = "DELETE FROM persona WHERE id_persona=?;";
    private static final String SQL_SELECT_FIND_BY_ID = "SELECT id_persona, nombre, apellido, email, telefono FROM persona WHERE id_persona=?;";
    private Connection conexion;
    private PreparedStatement preparedStatement;
    private ResultSet resultado;
    private Persona persona;
    List<Persona> personas;

    @Override
    public int insertar(Object nuevo) {
        int registros = 0;
        persona = (Persona) nuevo;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, persona.getNombre());
            preparedStatement.setString(2, persona.getApellido());
            preparedStatement.setString(3, persona.getEmail());
            preparedStatement.setLong(4, persona.getTelefono());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al insertar el registro: " + persona + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
        }
        return registros;
    }

    @Override
    public List listar() {
        personas = new ArrayList<>();
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT);
            resultado = preparedStatement.executeQuery();
            while (resultado.next()){
                persona = new Persona();
                persona.setIdPersona(resultado.getInt("id_persona"));
                persona.setNombre(resultado.getString("nombre"));
                persona.setApellido(resultado.getString("apellido"));
                persona.setEmail(resultado.getString("email"));
                persona.setTelefono(resultado.getLong("telefono"));
                personas.add(persona);
            }
            try{
                Conexion.close(resultado);
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            }catch (Exception exception){
                System.out.println("Error: " + exception.getMessage());
            }
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        return personas;
    }

    @Override
    public int actualizar(Object objeto) {
        int registros = 0;
        Persona persona = (Persona) objeto;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_UPDATE);
            preparedStatement.setString(1, persona.getNombre());
            preparedStatement.setString(2, persona.getApellido());
            preparedStatement.setString(3, persona.getEmail());
            preparedStatement.setLong(4, persona.getTelefono());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro: " + persona + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
        }
        return registros;
    }

    @Override
    public int eliminar(int id) {//Con objeto: Object objeto
        //Persona persona = (Persona) objeto;
        int registros = 0;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_DELETE);
            preparedStatement.setInt(1, id);
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro de indice: " + id + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
        }
        return registros;
    }

    @Override
    public Object buscarPorId(int id) {
        persona = new Persona();
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, id);
            resultado = preparedStatement.executeQuery();
            while (resultado.next()){
                persona.setIdPersona(resultado.getInt("id_persona"));
                persona.setNombre(resultado.getString("nombre"));
                persona.setApellido(resultado.getString("apellido"));
                persona.setEmail(resultado.getString("email"));
                persona.setTelefono(resultado.getLong("telefono"));
            }
            try{
                Conexion.close(resultado);
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            }catch (Exception exception){
                System.out.println("Error: " + exception.getMessage());
            }
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        return persona;
    }

    @Override
    public Object buscar(Object objeto) {
        persona = (Persona)objeto;
        try{
            conexion = getConnection();
            preparedStatement = conexion.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, persona.getIdPersona());
            resultado = preparedStatement.executeQuery();
            while (resultado.next()){
                persona.setIdPersona(resultado.getInt("id_persona"));
                persona.setNombre(resultado.getString("nombre"));
                persona.setApellido(resultado.getString("apellido"));
                persona.setEmail(resultado.getString("email"));
                persona.setTelefono(resultado.getLong("telefono"));
            }
            try{
                Conexion.close(resultado);
                Conexion.close(preparedStatement);
                Conexion.close(conexion);
            }catch (Exception exception){
                System.out.println("Error: " + exception.getMessage());
            }
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        return persona;
    }
}
