package datos;

import java.sql.*;

public class Conexion {
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/javaTest?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
    private static final String JDBC_USER = "usuarioTest";
    private static final String JDBC_PASSWORD = "tu_clave";

    public static void close(ResultSet resultado) throws SQLException {
        resultado.close();
    }

    public static void close(Statement instruccion) throws SQLException {
        instruccion.close();
    }

    public static void close(PreparedStatement smtm) throws SQLException {
        smtm.close();
    }

    public static void close(Connection conexion) throws SQLException {
        conexion.close();
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
    }
}
