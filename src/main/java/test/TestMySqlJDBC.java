package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestMySqlJDBC {
    public static void main(String[] args) {
        var url = "jdbc:mysql://localhost:3306/test?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        var user = "usuarioTest";
        var password = "tu_clave";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conexion = DriverManager.getConnection(url, user, password);
            Statement instruccion = conexion.createStatement();
            String sql = "SELECT id_persona, nombre, apellido, email, telefono FROM persona;";
            ResultSet resultado = instruccion.executeQuery(sql);
            while(resultado.next()){
                System.out.printf("ID Persona: %d, Nombre: %s, Apellido: %s, Email: %s, Teléfono: %d.\n",
                        resultado.getInt("id_persona"), resultado.getString("nombre"),
                        resultado.getString("apellido"), resultado.getString("email"),
                        resultado.getLong("telefono"));
            }
            resultado.close();
            instruccion.close();
            conexion.close();
        }catch(Exception exception){
            exception.printStackTrace(System.out);
        }
    }
}
