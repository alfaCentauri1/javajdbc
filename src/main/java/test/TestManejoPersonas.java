package test;

import datos.PersonaDAO;
import domain.Persona;

import java.util.List;

public class TestManejoPersonas {
    public static void main(String[] args) {
        PersonaDAO personaDAO = new PersonaDAO();
        Persona nuevaPersona = new Persona("Alejandra", "Gomez", "agomez@dominio.com", 54123456789L);
        Persona nuevaPersona2 = new Persona("María", "Gomez", "agomez@dominio.com", 54123456789L);
        Persona nuevaPersona3 = new Persona("Julia", "Gomez", "agomez@dominio.com", 54123456789L);
        personaDAO.insertar(nuevaPersona);
        personaDAO.insertar(nuevaPersona2);
        personaDAO.insertar(nuevaPersona3);
        List<Persona> listado = personaDAO.listar();
        for (Persona persona: listado){
            System.out.println(persona);
        }
        Persona encontrada = (Persona) personaDAO.buscarPorId(2);
        if (encontrada != null) {
            personaDAO.eliminar(2);
            System.out.println("Fue borrado el registro de la persona: " + encontrada);
        }
    }
}
